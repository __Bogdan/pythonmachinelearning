
import numpy as np
import re


class Expression:

    def __init__(self, expression_string = ""):
        # Possible cases:
        # - a + L
        # - a + bL
        # - bL + a
        # - bL
        # - a
        # - -a + L
        # - -a + bL
        # - -bL + a
        # - -bL
        # - -a

        # possible address bits:
        # - 0, 1 or 2 operations
        # - has oper at the begining
        # - has 'L'
        # - whether the operation is '-' or '+'

        # covers 'a' and '-a'
        if "L" not in expression_string:
            try:
                self._real = int(expression_string)
                self._imag = 0
                return
            except:
                raise Exception("'%s' can't be parsed to int" % expression_string)

        # trim the expression
        expression_string = expression_string.replace(" ", "")

        m = re.compile("[\+\-]{1}").match(expression_string)
        if m:
            operation_char = expression_string[m.start()]

            args = expression_string.split(operation_char)
            args.sort(key=lambda x:("L" in x))
            args = [a for a in args]

            # TODO


    def __add__ (self, x):
        pass

class EigenData:

    def __init__(self, matrix):
        self._matrix = np.array(matrix)
        self.calculate_data()

    def calculate_data(self):
        # get koefs for all labda powers
        # - decompose until matrixes have gone
        # - get rid of multiplications
        # - summarise foreach power

        # resolve the equasion

class DimensionalityReduction:
    pass
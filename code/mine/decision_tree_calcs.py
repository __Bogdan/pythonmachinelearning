#!/usr/bin/env python

import numpy as np


class DecisionTreeNode(object):

    instance_count = 0

    def __init__(self, class_samples_array, label=str(instance_count), rchild=None, lchild=None, parent=None):
        self.class_samples_array = np.array(class_samples_array)
        self.rchild = rchild
        self.lchild = lchild
        self.parent = parent
        self.label = label

        DecisionTreeNode.instance_count += 1

    def __str__(self):
        return self.label

    def add_rchild(self, child):
        self.rchild = child
        child.parent = self

    def add_lchild(self, child):
        self.lchild = child
        child.parent = self

    @property
    def sample_percentage(self):
        if self.parent is None:
            raise Exception("Attempt to call sample percentage on tree root %s" % self)
        return float(self.class_samples_array.sum()) / float(self.parent.class_samples_array.sum())

    @property
    def node_classification_error(self):
        return 1.0 - (float(self.class_samples_array.max()) / float(self.class_samples_array.sum()))

    @property
    def tree_classification_error(self):
        if (self.rchild is None) != (self.lchild is None):
            raise Exception("%s: The right child is %s and the left one is %s" % (self, self.rchild, self.lchild))

        return self.node_classification_error \
               - self.rchild.sample_percentage * self.rchild.node_classification_error \
               - self.lchild.sample_percentage * self.lchild.node_classification_error


A = DecisionTreeNode([40, 40])
A.add_rchild(DecisionTreeNode([10, 30]))
A.add_lchild(DecisionTreeNode([30, 10]))

B = DecisionTreeNode([40, 40])
B.add_rchild(DecisionTreeNode([20, 0]))
B.add_lchild(DecisionTreeNode([20, 40]))


print("Classification errors: A = %s, B = %s" % (A.tree_classification_error, B.tree_classification_error))